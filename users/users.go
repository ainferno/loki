package users

import (
	"fmt"

	"gorm.io/gorm"
)

type User struct {
	ID       int    `json:"id"`
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

type Users struct {
	db *gorm.DB
}

func New(db *gorm.DB) *Users {
	return &Users{db}
}

func (u Users) Find() []User {
	var users []User
	result := u.db.Find(&users)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return users
}

func (u Users) Create(user User) User {
	result := u.db.Create(&user)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return user
}

func (u Users) Read(id int) User {
	var user User
	result := u.db.First(&user, id)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return user
}

func (u Users) Update(user User, id int) error {
	var tmp User
	result := u.db.First(&tmp, id)
	if result.Error != nil {
		return result.Error
	}

	user.ID = id
	result = u.db.Model(&user).Updates(user)
	return result.Error
}

func (u Users) Delete(id int) {
	result := u.db.Delete(&User{}, id)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
}
