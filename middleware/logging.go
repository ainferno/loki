package middleware

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Println("Error reading body", err)
		}
		// defer r.Body.Close()

		start := time.Now()
		next.ServeHTTP(rw, r)
		msg := fmt.Sprintf(
			"method: %s, url: %s, body: %s, duration: %d milliseconds",
			r.Method,
			r.RequestURI,
			strings.ReplaceAll(string(body), " ", ""),
			time.Since(start)/1e6,
		)
		log.Println(msg)
	})
}
