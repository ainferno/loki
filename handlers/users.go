package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/mwazovzky/loki/users"
	"gorm.io/gorm"
)

type Users struct {
	users     *users.Users
	validator *validator.Validate
}

func New(db *gorm.DB, v *validator.Validate) *Users {
	users := users.New(db)
	return &Users{users, v}
}

func (u *Users) Index(rw http.ResponseWriter, r *http.Request) {
	log.Println("Users Index Page")

	data := u.users.Find()

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err := e.Encode(data)
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

func (u *Users) Show(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		panic(err)
	}

	data := u.users.Read(id)

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err = e.Encode(data)
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

func (u *Users) Create(rw http.ResponseWriter, r *http.Request) {
	var user users.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(rw, "Unable to decode user", http.StatusInternalServerError)
		return
	}

	err = u.validator.Struct(user)
	if err != nil {
		http.Error(rw, "Validation error", http.StatusUnprocessableEntity)
		return
	}

	user = u.users.Create(user)

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err = e.Encode(user)
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

func (u *Users) Delete(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		panic(err)
	}

	u.users.Delete(id)

	rw.WriteHeader(http.StatusNoContent)
}

func (u *Users) Update(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		panic(err)
	}

	var user users.User
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(rw, "Unable to decode user", http.StatusInternalServerError)
	}

	err = u.validator.Struct(user)
	if err != nil {
		http.Error(rw, "Validation error", http.StatusUnprocessableEntity)
		return
	}

	u.users.Update(user, id)

	rw.WriteHeader(http.StatusNoContent)
}
